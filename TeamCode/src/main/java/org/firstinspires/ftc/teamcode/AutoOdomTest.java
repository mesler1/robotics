
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.List;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

@Autonomous(name="AutoOdomTest", group="Pyro")

public class AutoOdomTest extends LinearOpMode {

    PyroHardware        robot = new PyroHardware(telemetry, this);
    PyroDrive            drive = new PyroDrive(robot);

    private ElapsedTime    runtime = new ElapsedTime();

    static final double    DRIVE_SPEED            = 0.8;
    static final double    DRIVE_SPEED_INCR    = 0.05;

    int target = 0;
    double target_start = 0;
    double speed = 0.;
    double angle = 0.;
    double rotate = 0.;
    boolean touched = false;

    @Override
    public void runOpMode()
    {

        robot.init(hardwareMap, true, true);
        waitForStart();
        robot.odom.targetX = 20000;
        robot.odom.targetY = 20000;

        while(opModeIsActive()) {
            robot.odom.updatePosition();
            robot.odom.setCurrentDirection();
            drive.runDrivePolarJoy(robot.odom.directionX / 5, robot.odom.directionY / 5, 0);
        }

    } // end of runOpMode

} //end of OpMode
