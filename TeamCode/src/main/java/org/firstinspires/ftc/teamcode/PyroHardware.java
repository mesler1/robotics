package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.Telemetry;
//import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.*;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.HardwareMap;
//import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.Servo;

/*
 * Motor channel:  Left  front drive motor: "left_front_drive"
 * Motor channel:  Right front drive motor: "right_front_drive"
 * Motor channel:  Left  back  drive motor: "left_back_drive"
 * Motor channel:  Right back  drive motor: "right_back_drive"
 * Motor channel:  Right front drive motor: "elevator"
 * Servo channel:  Servo to dump marker: "marker"
 * Servo channel:  Servo for left  foundation grabber: "left_found_servo"
 * Servo channel:            right foundation grabber: "right_found_servo"
 */

public class PyroHardware
{
  /* Public OpMode members. */
    public DcMotor    left_front_drive = null;
    public DcMotor    right_front_drive = null;
    public DcMotor    left_back_drive = null;
    public DcMotor    right_back_drive = null;
    public BNO055IMU  imu = null;
    public PyroGyro gyro;
    public PyroOdom odom;
    Telemetry telemetry;
    public LinearOpMode activeOpMode = null;
    public DcMotor    shooterRear = null;
    public DcMotor    shooterFront= null;
    public Servo      gate = null;
    public CRServo    intake = null;
    public CRServo    conveyor = null;

    /* local OpMode members. */
    HardwareMap hwMap           =  null;
    private ElapsedTime period  = new ElapsedTime();
    // Odometry variables
    public DcMotor verticalLeft, verticalRight, horizontal;
    //Hardware Map Names for drive motors and odometry wheels. THIS WILL CHANGE ON EACH ROBOT, YOU NEED TO UPDATE THESE VALUES ACCORDINGLY
    String rfName = "right_front_drive", rbName = "right_back_drive", lfName = "left_front_drive", lbName = "left_back_drive";
    String verticalLeftEncoderName = lbName, verticalRightEncoderName = rfName, horizontalEncoderName = lfName;

    /* Constructor */
    public PyroHardware(Telemetry telemetry, LinearOpMode opMode){
      this.telemetry = telemetry;
      activeOpMode = opMode;
    }
    public PyroHardware(Telemetry telemetry) {
      this.telemetry = telemetry;
    }
    public void init(HardwareMap ahwMap) {
      this.init(ahwMap, false, false);
    }
    /* Initialize Hardware interfaces */
    public void init(HardwareMap ahwMap, boolean useGyro, boolean useOdom) {
      // Save reference to Hardware map
      hwMap = ahwMap;

      // Define and Initialize Motors
      hwMap.logDevices();
      left_front_drive = hwMap.get(DcMotor.class, "left_front_drive");
      right_front_drive = hwMap.get(DcMotor.class, "right_front_drive");
      left_back_drive = hwMap.get(DcMotor.class, "left_back_drive");
      right_back_drive = hwMap.get(DcMotor.class, "right_back_drive");
      imu = hwMap.get(BNO055IMU.class, "imu");
      //initIMU();

      left_front_drive.setDirection(DcMotor.Direction.FORWARD);
      right_front_drive.setDirection(DcMotor.Direction.REVERSE);
      left_back_drive.setDirection(DcMotor.Direction.FORWARD);
      right_back_drive.setDirection(DcMotor.Direction.REVERSE);

      left_front_drive.setPower(0);
      right_front_drive.setPower(0);
      left_back_drive.setPower(0);
      right_back_drive.setPower(0);

      left_front_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      right_front_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      left_back_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      right_back_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

      left_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      right_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      left_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      right_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

       // Odometry related stuffs
      verticalLeft = hwMap.get(DcMotor.class, verticalLeftEncoderName);
      verticalRight = hwMap.get(DcMotor.class, verticalRightEncoderName);
      horizontal = hwMap.get(DcMotor.class, horizontalEncoderName);
      verticalLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      verticalRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      horizontal.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

      verticalLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      verticalRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      horizontal.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      // Odometry requires Gyro
      if (useGyro || useOdom) {
        gyro = new PyroGyro(this);
        gyro.init();
      }
      if (useOdom) {
        odom = new PyroOdom(this);
      }
      /*
      // Define and Initialize Motors
      shooterFront = hwMap.get(DcMotor.class, "shooterFront");
      shooterRear = hwMap.get(DcMotor.class, "shooterRear");
      intake = hwMap.get(CRServo.class, "intake");
      conveyor = hwMap.get(CRServo.class, "conveyor");
      gate = hwMap.get(Servo.class, "gate");

      shooterFront.setDirection(DcMotor.Direction.REVERSE);
      shooterRear.setDirection(DcMotor.Direction.REVERSE);

      shooterFront.setPower(0);
      shooterRear.setPower(0);

      shooterFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
      shooterRear.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

      shooterFront.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      shooterRear.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
      telemetry.addData("PyroHardware", "Init Complete.");
      */
    }

  public void initIMU() {
      imu = hwMap.get(BNO055IMU.class, "imu");
      BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
      parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
      parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
      parameters.calibrationDataFile = "BNO055IMUCalibration.json";
      imu.initialize(parameters);
      parameters.loggingEnabled = true;
      parameters.loggingTag = "IMU";
      parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
      imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);
  }
}
