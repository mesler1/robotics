package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;

//import org.firstinspires.ftc.robotcore.external.Telemetry;
import com.qualcomm.hardware.bosch.BNO055IMU;
import org.firstinspires.ftc.robotcore.external.Func;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

import java.util.Locale;

public class PyroGyro {

    PyroHardware robot ;

    public float target = 0;

    Orientation angles;
    Acceleration gravity;
    double zAccumulated;  //Total rotation left/right
    private double TURNSPEED = 0.7;
    double rotate = 0.35;

    /* Constructor */
    public PyroGyro(PyroHardware robot){
        this.robot = robot;
    }

    /* Initialize standard Hardware interfaces */
    public void init() {

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.RADIANS;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        // Retrieve and initialize the IMU. We expect the IMU to be attached to an I2C port
        // on a Core Device Interface Module, configured to be a sensor of type "AdaFruit IMU",
        // and named "imu".

        robot.imu.initialize(parameters);
        target = getHeadingFloat();

        while (!robot.imu.isGyroCalibrated()) {
            robot.telemetry.addData("Calibration state", "Waiting for calibration");
            robot.telemetry.update();
            //robot.activeOpMode.idle();
        }
        robot.telemetry.addData("Calibration state", robot.imu.isGyroCalibrated());
        robot.telemetry.update();
    }

    /**
     * Returns the current angle (in degrees) of the gyro.
     */
    public double getHeading() {
        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        // return Math.round(angles.firstAngle);
        return angles.firstAngle;
    }

    public float getHeadingFloat() {
        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        // return Math.round(angles.firstAngle);
        return angles.firstAngle;
    }

    /**
     * Returns the difference between the target and the current heading.
     */
    public double getError(double target) {
        double robotError = target - (double) getHeading();
        while (robotError > 180) robotError -= 360;
        while (robotError <= -180) robotError += 360;
        return robotError;
    }

    //private ElapsedTime runtime = new ElapsedTime();

    //This function turns a number of degrees compared to where the robot was when the program started.
    //Positive numbers turn left.
    public void turnAbsolute(int target, ElapsedTime runtime, double timelimit) {
        robot.left_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.right_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.left_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.right_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        zAccumulated = getHeading();

        while (runtime.seconds() < timelimit && Math.abs(zAccumulated - target) > 3 ) {
            rotate = TURNSPEED * Math.max(0.1, Math.min(Math.abs(zAccumulated - target) / 20., 1.)) ;

            if (zAccumulated > target) {  // we need to turn right
                robot.left_front_drive.setPower(rotate);
                robot.right_front_drive.setPower(-rotate);
                robot.left_back_drive.setPower(rotate);
                robot.right_back_drive.setPower(-rotate);
            }

            if (zAccumulated <= target) {  // we need to turn left
                robot.left_front_drive.setPower(-rotate);
                robot.right_front_drive.setPower(rotate);
                robot.left_back_drive.setPower(-rotate);
                robot.right_back_drive.setPower(rotate);
            }

            zAccumulated = getHeading();
            robot.telemetry.addData("current heading", "%03d", zAccumulated);
            robot.telemetry.update();
        }

        robot.left_front_drive.setPower(0);
        robot.right_front_drive.setPower(0);
        robot.left_back_drive.setPower(0);
        robot.right_back_drive.setPower(0);
    }

    public double getHeadingRadians() {
        angles = robot.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.RADIANS);
        return (double) angles.firstAngle;
    }
}
