package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.hardware.bosch.BNO055IMU;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

public class PyroDrive {

    PyroHardware robot;
    LinearOpMode opmode;

    static final double COUNTS_PER_MOTOR_REV     = 383.6;
    static final double DRIVE_GEAR_REDUCTION    = 2.0;
    static final double WHEEL_DIAMETER_INCHES   = 4.;
    static final double COUNTS_PER_INCH         =
            (COUNTS_PER_MOTOR_REV *
            DRIVE_GEAR_REDUCTION) /
            (WHEEL_DIAMETER_INCHES *
            Math.PI);

    private double TURNSPEED         = 0.35;
    private double DRIVE_SPEED       = 0.3;
    private double STRAFE_SPEED      = 0.8;
    private double DRIVE_SPEED_INCR  = 0.01;
    private double MAXSPEED          = 0.8;
    private double MIDSPEED          = 0.6;
    private double MINSPEED          = 0.1;
    private double P_DRIVE_COEFF_1   = 0.01;
    private double P_DRIVE_COEFF_2   = 0.25;
    private double deltaTime         = 0;
    private double lastTime          = 0;
    private double localRunTime      = 0;

    /* Constructor */
    public PyroDrive(PyroHardware robot) {
        this.robot = robot;
    }

    /* Initialize standard Hardware interfaces */
    public void init() {

    }

    public void runDrivePolarEasy(double leftX, double leftY, double rightX) {
        double newX = Math.pow(leftX, 3.);
        double newY = Math.pow(leftY, 3.);
        double speed = Math.hypot(newX, newY);
        double angle = Math.atan2(-newY, newX); // - Math.PI / 4;
        double rotate = rightX;
        runDrivePolar(speed * MAXSPEED, angle, rotate);
    }

    public void runDrivePolarJoy(double leftX, double leftY, double rightX) {
        double speed = Math.hypot(leftX, leftY);
        double angle = Math.atan2(-leftY, leftX); // - Math.PI / 4;
        double rotate = rightX;
        runDrivePolar(speed * MAXSPEED, angle, rotate);
    }

    /** Drive without jerking */
    public void runDrivePolarJoySmooth(double leftX, double leftY, double rightX, OpMode opMode) {
        double speed = Math.hypot(leftX, leftY);
        if (lastTime == 0) {
            lastTime = opMode.time;
        }
        if (speed < 0.1) {
            deltaTime = 0;
        }
        else {
            deltaTime += opMode.time - lastTime;
        }
        if (deltaTime < 0.5) {
            speed = (Math.sin(deltaTime))  * speed;
        }
        // double smoothSpeed = Math.sin(deltaTime) * speed;
        double angle = Math.atan2(-leftY, leftX); // - Math.PI / 4;
        double rotate = rightX;
        runDrivePolar(speed * MAXSPEED, angle, rotate);
        lastTime = opMode.time;
    }

    /**
    * To pivot in place, set speed to 0
     */
    public void runDrivePolar(double speed, double angle, double rotate) {
        double goAngle = angle - Math.PI / 4.;
        double v1 = speed * Math.cos(goAngle) + rotate;
        double v2 = speed * Math.sin(goAngle) - rotate;
        double v3 = speed * Math.sin(goAngle) + rotate;
        double v4 = speed * Math.cos(goAngle) - rotate;
        robot.left_front_drive.setPower(v1);
        robot.right_front_drive.setPower(v2);
        robot.left_back_drive.setPower(v3);
        robot.right_back_drive.setPower(v4);
    }

    public void runStrafe(boolean right, double seconds,
            LinearOpMode opmode, ElapsedTime runtime) {
        this.opmode = opmode;
        double speed = 0.;
        double rotate = 0.;
        double angle = 0.;
        if (! right) {
            angle = Math.PI;
        }
        noencoder();
        speed = 0.;
        runtime.reset();
        while (opmode.opModeIsActive() &&
               (runtime.seconds() < seconds)) {
            if (Math.abs(speed) < STRAFE_SPEED) {
                speed = speed + DRIVE_SPEED_INCR;
            }
            runDrivePolar(speed, angle, rotate);
        }
        stop();
    }

       public void runStraightSlow(boolean forward, double seconds,
            LinearOpMode opmode, ElapsedTime runtime) {
        this.opmode = opmode;
        double speed = 0.;
        double rotate = 0.;
        double angle = Math.PI/2.;
        if (! forward) {
            angle = -Math.PI/2.;
        }
        noencoder();
        speed = 0.2;
        runtime.reset();
        while (opmode.opModeIsActive() &&
               (runtime.seconds() < seconds)) {
            runDrivePolar(speed, angle, rotate);
        }
        stop();
    }
    
       public void runStraight(boolean forward, double seconds,
            LinearOpMode opmode, ElapsedTime runtime) {
        this.opmode = opmode;
        double speed = 0.;
        double rotate = 0.;
        double angle = Math.PI/2.;
        if (! forward) {
            angle = -Math.PI/2.;
        }
        noencoder();
        speed = 0.;
        runtime.reset();
        while (opmode.opModeIsActive() &&
               (runtime.seconds() < seconds)) {
            if (Math.abs(speed) < STRAFE_SPEED) {
                speed = speed + DRIVE_SPEED_INCR;
            }
            runDrivePolar(speed, angle, rotate);
        }
        stop();
    }

    public void runDriveStrafeGyro(boolean right, PyroGyro gyro, LinearOpMode opmode, ElapsedTime runtime, double timelimit) {
        this.opmode = opmode;
        double speed = 0.;
        boolean aggressive = false;
        double targetAngle = gyro.getHeading();
        speed = MINSPEED;
        runtime.reset();
        double angle = 0.;
        double rotate = 0.;
        if (! right) {
            angle = Math.PI / 2;
        }
        while (opmode.opModeIsActive() &&
                (runtime.seconds() < timelimit) ) {
            if (Math.abs(speed) < MAXSPEED) {
                speed = speed + DRIVE_SPEED_INCR;
            }
            // adjust relative speed based on heading
            double error = gyro.getError(targetAngle);
            double steer = getSteer(error, (aggressive?P_DRIVE_COEFF_2:P_DRIVE_COEFF_1) );
            // if driving in reverse, the motor correction also needs to be reversed
            // Adjust motor powers for heading correction
            rotate  = - steer;
            runDrivePolar(speed, angle, rotate);
            // Allow time for other processes to run.
            opmode.sleep(1);;
        }
        stop();
    }



    public void runDriveForwardEncoder(double distance,
            LinearOpMode opmode, ElapsedTime runtime, double timeout) {
        this.opmode = opmode;
        double speed = 0.;
        double direction = 1.;
        int newLeftTarget = 0;
        int newRightTarget = 0;
        int newLeftTargetF = 0;
        int newRightTargetF = 0;
        direction = 1.;
        if (distance < 0.) {
            direction = -1.;
        }
        newLeftTarget = robot.left_back_drive.getCurrentPosition();
        newRightTarget = robot.right_back_drive.getCurrentPosition();
        newLeftTargetF = robot.left_front_drive.getCurrentPosition();
        newRightTargetF = robot.right_front_drive.getCurrentPosition();
        robot.left_back_drive.setTargetPosition(newLeftTarget);
        robot.right_back_drive.setTargetPosition(newRightTarget);
        robot.left_front_drive.setTargetPosition(newLeftTargetF);
        robot.right_front_drive.setTargetPosition(newRightTargetF);
        runtoposition();
        robot.telemetry.addData("Start",  "from %7d %7d", 
            robot.left_back_drive.getCurrentPosition(),
            robot.right_back_drive.getCurrentPosition());
        newLeftTarget = robot.left_back_drive.getCurrentPosition() + 
            (int)(distance * COUNTS_PER_INCH);
                opmode.sleep(1);
        newRightTarget = robot.right_back_drive.getCurrentPosition() +
            (int)(distance * COUNTS_PER_INCH);
                opmode.sleep(1);
        newLeftTargetF = robot.left_front_drive.getCurrentPosition() + 
            (int)(distance * COUNTS_PER_INCH);
                opmode.sleep(1);
        newRightTargetF = robot.right_front_drive.getCurrentPosition() +
            (int)(distance * COUNTS_PER_INCH);
                opmode.sleep(1);
        robot.left_back_drive.setTargetPosition(newLeftTarget);
        robot.right_back_drive.setTargetPosition(newRightTarget);
        robot.left_front_drive.setTargetPosition(newLeftTargetF);
        robot.right_front_drive.setTargetPosition(newRightTargetF);
        robot.telemetry.addData("Path","to %7d %7d",newLeftTarget,newRightTarget);
        robot.telemetry.update();
        speed = MINSPEED;
        runtime.reset();
        while (opmode.opModeIsActive() &&
               (runtime.seconds() < timeout) &&
               (robot.left_back_drive.isBusy() &&
               robot.right_back_drive.isBusy() &&
               robot.left_front_drive.isBusy() &&
               robot.right_front_drive.isBusy() )) {
            
            double left_togo = newLeftTarget - robot.left_back_drive.getCurrentPosition();
            double right_togo = newRightTarget - robot.right_back_drive.getCurrentPosition();
            double err = Math.abs(0.5*(left_togo+right_togo)/COUNTS_PER_INCH);
            if (err < 5) {
                speed = MINSPEED + err/5.*(MIDSPEED-MINSPEED);
            } else if (speed < MIDSPEED) {
                speed = speed + DRIVE_SPEED_INCR;
            }
            robot.left_front_drive.setPower(direction*speed);
            robot.right_front_drive.setPower(direction*speed);
            robot.left_back_drive.setPower(direction*speed);
            robot.right_back_drive.setPower(direction*speed);
            robot.telemetry.addData("Running",  "at %7d %7d speed %f", 
            robot.left_back_drive.getCurrentPosition(),
            robot.right_back_drive.getCurrentPosition(),
            speed);
            

        }
        stop();
    }

    public double getSteer(double error, double PCoeff) {
        return Range.clip(error * PCoeff, -1, 1);
    }

    public void runDriveCartJoy(double leftX, double leftY, double rightX) {
        double forward = -leftY;
        double turn = leftX;
        double strafe = rightX;
        runDriveCart(forward, turn, strafe);
    }

    public void runDriveCart(double forward, double turn, double strafe) {
        double v1 = forward + turn - strafe;
        double v2 = forward - turn + strafe;
        double v3 = forward + turn + strafe;
        double v4 = forward - turn - strafe;
        robot.left_front_drive.setPower(v1);
        robot.right_front_drive.setPower(v2);
        robot.left_back_drive.setPower(v3);
        robot.right_back_drive.setPower(v4);
    }

    public void runtoposition() {
        robot.left_front_drive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.right_front_drive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.left_back_drive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.right_back_drive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void withencoder() {
        robot.left_front_drive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.right_front_drive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.left_back_drive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.right_back_drive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void noencoder() {
        robot.left_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.right_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.left_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.right_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public void stop() {
        robot.left_front_drive.setPower(0.);
        robot.right_front_drive.setPower(0.);
        robot.left_back_drive.setPower(0.);
        robot.right_back_drive.setPower(0.);
    }
   

}
