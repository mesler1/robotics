
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.List;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

@TeleOp(name="IdleTest", group="Pyro")

public class IdleTest extends LinearOpMode {

    PyroHardware        robot = new PyroHardware(telemetry, this);
    PyroDrive           drive = new PyroDrive(robot);
    // PyroOdom            odom = new PyroOdom(robot);

    private ElapsedTime    runtime = new ElapsedTime();

    static final double    DRIVE_SPEED            = 0.8;
    static final double    DRIVE_SPEED_INCR    = 0.05;

    int target = 0;
    float target_start = 0;
    double speed = 0.;
    double angle = 0.;
    double rotate = 0.;
    boolean touched = false;
    double thetaLimit = 90;

    @Override
    public void runOpMode() {

        robot.init(hardwareMap, true, true);
        drive.noencoder();

        robot.telemetry.update();
        waitForStart();
        runtime.reset();

        while (!isStopRequested() && opModeIsActive()) {
            robot.odom.updatePosition();
            // robot.telemetry.addData("heading", robot.gyro.getHeading());
            robot.telemetry.addData("", "========== Odometry ==========");
            robot.telemetry.addData("left ticks", "%d", robot.verticalLeft.getCurrentPosition());
            robot.telemetry.addData("right ticks", "%d", robot.verticalRight.getCurrentPosition());
            robot.telemetry.addData("center ticks", "%d", robot.horizontal.getCurrentPosition());
            robot.telemetry.addData("X_Position","%3f",robot.odom.robotGlobalXCoordinatePosition);
            robot.telemetry.addData("Y_Position","%3f",robot.odom.robotGlobalYCoordinatePosition);

            robot.telemetry.addData("Odom Theta", robot.odom.robotOrientationRadians);
            robot.telemetry.addData("", "========== Gyro ==========");
            robot.telemetry.addData("Gyro Heading Radians", robot.gyro.getHeadingRadians());
            robot.telemetry.addData("Gyro Calibration", robot.imu.isGyroCalibrated());
            robot.telemetry.update();

        }

    }

} //end of OpMode
