
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.List;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

@Autonomous(name="AutoStrafe", group="Pyro")

public class AutoStrafe extends LinearOpMode {

    PyroHardware        robot = new PyroHardware(telemetry);
    PyroGyro            gyro = new PyroGyro(robot);
    PyroDrive            drive = new PyroDrive(robot);

    private ElapsedTime    runtime = new ElapsedTime();

    static final double    DRIVE_SPEED            = 0.8;
    static final double    DRIVE_SPEED_INCR    = 0.05;

    int target = 0;
    double target_start = 0;
    double speed = 0.;
    double angle = 0.;
    double rotate = 0.;
    boolean touched = false;

    @Override
    public void runOpMode()
    {

        robot.init(hardwareMap);
        gyro.init();

        boolean right = true;

        //init gyro
        target_start = gyro.getHeading();

        // Send telemetry message to signify robot waiting;
        robot.telemetry.addData("Status", "Waiting");    //
        robot.telemetry.addData("Heading", "%3d", target_start);    //
        robot.telemetry.update();
        waitForStart();

        sleep(500);

        runtime.reset();

        // Step 1:  drive forward
        // Step 2:  stop

        // Step 1:  drive forward
        robot.telemetry.addData("Step 1","");    //
        robot.telemetry.update();
        //drive.runDriveForwardEncoder(20., this, runtime);
        drive.runDriveStrafeGyro(right, gyro, this, runtime, 5.);
        drive.stop();
        sleep(100);

        // Step 2:  stop
        drive.stop();

    } // end of runOpMode

    /*
    public boolean iAmBlue() {
        return true;
    }
    */

} //end of OpMode
