package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.util.ReadWriteFile;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.internal.system.AppUtil;

import java.io.File;


public class PyroOdom {
    static final DcMotor.ZeroPowerBehavior ZERO_POWER_BEHAVIOR = DcMotor.ZeroPowerBehavior.BRAKE;
    private double horizontalEncoderTickPerDegreeOffset;
    public double robotEncoderWheelDistance;
    HardwareMap hwMap;
    ElapsedTime clock = new ElapsedTime();

    static final double oneRotationTicks = 1440;
    static final double wheelRadius = 0.019; // in meters
    static final double wheelRadiusInches = 0.74803;
    // static final double wheelDistanceApart = 0.2 + (0.048 * 3); // in meters
    //static final double wheelDistanceApart = 0.349;
    static final double wheelCircumferenceInches = wheelRadiusInches*2 * Math.PI;
    static final double COUNTS_PER_INCH = oneRotationTicks / wheelCircumferenceInches;
    static final double thetaMultiplier = 1;
    //final double COUNTS_PER_INCH = PyroOdom.oneRotationTicks / (PyroOdom.wheelRadiusInches * (Math.PI * 2));
    //final double COUNTS_PER_INCH = 307.699557;

    private int leftEncoderPos = 0;
    private int centerEncoderPos = 0;
    private int rightEncoderPos = 0;
    private double deltaLeftDistance = 0;
    private double deltaRightDistance = 0;
    private double deltaCenterDistance = 0;
    private double x = 0;
    private double y = 0;
    private double imuTheta = 0;
    private double thetaPrime = Math.PI / 2;
    private double totalLeftTicks = 0;
    private double totalRightTicks = 0;
    private double totalCenterTicks = 0;
    /**
     * plug left encoder into frontleft, right encoder into frontright, center encoder into backleft (arbitary assignments)
     */
    private DcMotor FR = null;
    private DcMotor FL = null;
    private DcMotor BR = null;
    private DcMotor BL = null;
    private DcMotor leftEncoderMotor = null;
    private DcMotor rightEncoderMotor = null;
    private DcMotor centerEncoderMotor = null;
    PyroHardware robot;
    private Orientation angles;
    private double verticalLeftEncoderWheelPosition = 0;
    private double verticalRightEncoderWheelPosition = 0;

    private double verticalLeftEncoderPositionMultiplier = 1;
    private double verticalRightEncoderPositionMultiplier = 1;
    private double normalEncoderPositionMultiplier = 1;

    private double previousVerticalLeftEncoderWheelPosition = 0;
    private double previousVerticalRightEncoderWheelPosition = 0;
    private double changeInRobotOrientation = 0;
    public double robotOrientationRadians = 0;
    private double normalEncoderWheelPosition = 0;
    private double prevNormalEncoderWheelPosition = 0;
    public double robotGlobalXCoordinatePosition = 0;
    public double robotGlobalYCoordinatePosition = 0;

    public double targetX = 0;
    public double targetY = 0;
    public double directionX = 0;
    public double directionY = 0;


    /* Constructor */
    public PyroOdom(PyroHardware robot) {
        this.robot = robot;
        File wheelBaseSeparationFile = AppUtil.getInstance().getSettingsFile("wheelBaseSeparation.txt");
        File horizontalTickOffsetFile = AppUtil.getInstance().getSettingsFile("horizontalTickOffset.txt");
        robotEncoderWheelDistance = Double.parseDouble(ReadWriteFile.readFile(wheelBaseSeparationFile).trim()) * COUNTS_PER_INCH;
        this.horizontalEncoderTickPerDegreeOffset = Double.parseDouble(ReadWriteFile.readFile(horizontalTickOffsetFile).trim());
    }


    public void updatePosition() {
        //Get Current Positions
        verticalLeftEncoderWheelPosition = (robot.verticalLeft.getCurrentPosition() * verticalLeftEncoderPositionMultiplier);
        verticalRightEncoderWheelPosition = (robot.verticalRight.getCurrentPosition() * verticalRightEncoderPositionMultiplier);

        double leftChange = verticalLeftEncoderWheelPosition - previousVerticalLeftEncoderWheelPosition;
        double rightChange = verticalRightEncoderWheelPosition - previousVerticalRightEncoderWheelPosition;

        //Calculate Angle
        changeInRobotOrientation = (leftChange - rightChange) / (robotEncoderWheelDistance);
        robotOrientationRadians = ((robotOrientationRadians + changeInRobotOrientation));

        //Get the components of the motion
        normalEncoderWheelPosition = (robot.horizontal.getCurrentPosition()*normalEncoderPositionMultiplier);
        double rawHorizontalChange = normalEncoderWheelPosition - prevNormalEncoderWheelPosition;
        double horizontalChange = rawHorizontalChange - (changeInRobotOrientation*horizontalEncoderTickPerDegreeOffset);

        double p = ((rightChange + leftChange) / 2);
        double n = horizontalChange;

        //Calculate and update the position values
        robotGlobalXCoordinatePosition = robotGlobalXCoordinatePosition + (p*Math.sin(robotOrientationRadians) + n*Math.cos(robotOrientationRadians));
        robotGlobalYCoordinatePosition = robotGlobalYCoordinatePosition + (p*Math.cos(robotOrientationRadians) - n*Math.sin(robotOrientationRadians));

        previousVerticalLeftEncoderWheelPosition = verticalLeftEncoderWheelPosition;
        previousVerticalRightEncoderWheelPosition = verticalRightEncoderWheelPosition;
        prevNormalEncoderWheelPosition = normalEncoderWheelPosition;
        totalLeftTicks += leftChange;
        totalRightTicks += rightChange;
        totalCenterTicks += rawHorizontalChange;
    }

    public double getTotalLeftTicks() {
        return totalLeftTicks;
    }
    public double getTotalRightTicks() {
        return totalRightTicks;
    }
    public double getTotalCenterTicks() {
        return totalCenterTicks;
    }
    public void setX(double _x) {
        x = _x;
    }
    public void setY(double _y) {
        y = _y;
    }

    public double getDeltaLeftDistance() {
        return deltaLeftDistance;
    }

    public double getDeltaRightDistance() {
        return deltaRightDistance;
    }



    public void reverseLeftEncoder(){
        if(verticalLeftEncoderPositionMultiplier == 1){
            verticalLeftEncoderPositionMultiplier = -1;
        }else{
            verticalLeftEncoderPositionMultiplier = 1;
        }
    }

    public void reverseRightEncoder(){
        if(verticalRightEncoderPositionMultiplier == 1){
            verticalRightEncoderPositionMultiplier = -1;
        }else{
            verticalRightEncoderPositionMultiplier = 1;
        }
    }

    public void reverseNormalEncoder(){
        if(normalEncoderPositionMultiplier == 1){
            normalEncoderPositionMultiplier = -1;
        }else{
            normalEncoderPositionMultiplier = 1;
        }
    }

    public void SetupMotorsForOdometry() {
        robot.right_front_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.right_back_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.left_front_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.left_back_drive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        robot.right_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.right_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.left_front_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.left_back_drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        robot.verticalLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.verticalRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.horizontal.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        robot.verticalLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.verticalRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.horizontal.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        //  If we reverse an encoder, we need to be mindful of the encoderMultiplier we use!
        // In our case, we reverse the verticalLeft encoder (right front)
        robot.right_front_drive.setDirection(DcMotorSimple.Direction.REVERSE);
        robot.right_back_drive.setDirection(DcMotorSimple.Direction.REVERSE);


        /*
        robot.right_front_drive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.right_back_drive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.left_front_drive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.left_back_drive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        */
    }
    public double getCurrentDeltaX() {
        return targetX - robotGlobalXCoordinatePosition;
    }
    public double getCurrentDeltaY() {
        return targetY - robotGlobalYCoordinatePosition;
    }
    public double getMagnitude(double x, double y) {
        return Math.sqrt((x*x) + (y*y));
    }
    public void setCurrentDirection() {
        double dX = getCurrentDeltaX();
        double dY = getCurrentDeltaY();
        double magnitude = getMagnitude(dX,dY);
        directionX = dX/magnitude;
        directionY = dY/magnitude;
    }
}
