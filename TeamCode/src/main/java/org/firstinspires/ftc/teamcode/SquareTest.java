
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.List;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

@Autonomous(name="SquareTest", group="Pyro")

public class SquareTest extends LinearOpMode {

    PyroHardware        robot = new PyroHardware(telemetry, this);
    PyroDrive           drive = new PyroDrive(robot);
    // PyroOdom            odom = new PyroOdom(robot);

    private ElapsedTime    runtime = new ElapsedTime();

    static final double    DRIVE_SPEED            = 0.8;
    static final double    DRIVE_SPEED_INCR    = 0.05;

    int target = 0;
    float target_start = 0;
    double speed = 0.;
    double angle = 0.;
    double rotate = 0.;
    boolean touched = false;
    double thetaLimit = 90;
    private final double PIVOT_SPEED = -0.25;
    private double timer = 0;

    @Override
    public void runOpMode()
    {

        robot.init(hardwareMap, true, true);

        double deltaTime = 0;
        double lastTime = 0;

        drive.noencoder();

        boolean right = false;

        //init gyro
        target_start = robot.gyro.getHeadingFloat();

        // Send telemetry message to signify robot waiting;
        robot.telemetry.addData("Status", "Waiting");    //
        // robot.telemetry.addData("Heading", "%3d", target_start);    //
        robot.telemetry.update();
        waitForStart();

        sleep(500);

        runtime.reset();

        // Step 1:  drive forward
        // Step 2:  stop

        // Step 1:  drive forward
        robot.telemetry.addData("Step 1","");    //
        robot.telemetry.update();
        //drive.runDriveForwardEncoder(20., this, runtime);
        // drive.runDriveStrafeGyro(right, gyro, this, runtime, 5.);
        lastTime = runtime.time();
        for(int i = 0; i <=3; i++) {
            timer = 1f;
            // drive.runStraightSlow(true, 2, this, runtime);
            setPowerAll(0.3,0.3,0.3,0.3);
            while(timer >= 0) {
                autotelemetry("Now Driving");

                deltaTime = runtime.time() - lastTime;
                timer -= deltaTime;
                lastTime = runtime.time();
            }
            //drive.runDrivePolar(0.4, 0, 0);
            /*
            while (robot.odom.getY() < 0.5) {
                sleep(1);
                robot.telemetry.addData("heading", robot.gyro.getHeading());
                robot.telemetry.addData("x", robot.odom.getX());
                robot.telemetry.addData("y", robot.odom.getY());
                robot.telemetry.addData("Runtime", runtime);
                robot.telemetry.update();
                robot.odom.updatePosition();
            }
            */
            robot.telemetry.addData("Status", "Now Stopping");
            robot.telemetry.update();
            drive.stop();

            // drive.runDrivePolar(0, 0, -0.1);

            while (Math.abs(robot.odom.robotOrientationRadians) <= (Math.PI / 2) * (i + 1)) {
                if(robot.gyro.getHeading() < 60) {
                    setPowerAll(-PIVOT_SPEED, -PIVOT_SPEED, PIVOT_SPEED, PIVOT_SPEED);
                }else{
                    setPowerAll(-PIVOT_SPEED/2, -PIVOT_SPEED/2, PIVOT_SPEED/2, PIVOT_SPEED/2);
                }
                // sleep(1);
                autotelemetry("Now Turning");
                /*
                robot.telemetry.addData("Status", "Now Turning");
                robot.telemetry.addData("", "========== Odometry ==========");
                // robot.telemetry.addData("heading", robot.gyro.getHeading());
                robot.telemetry.addData("Left Ticks", robot.odom.getLeftTicks());
                robot.telemetry.addData("Right Ticks", robot.odom.getRightTicks());
                robot.telemetry.addData("Odom Theta", robot.odom.robotOrientationRadians);
                robot.telemetry.addData("", "========== Gyro ==========");
                robot.telemetry.addData("Gyro Heading Radians", robot.gyro.getHeadingRadians());
                robot.telemetry.addData("getCallibration", robot.imu.isGyroCalibrated());
                robot.telemetry.addData("", "========== Misc ==========");
                robot.telemetry.addData("Pi over 2 times I plus 1", (Math.PI / 2) * (i + 1));

                robot.telemetry.update();
                */
                robot.odom.updatePosition();
            }
            // Step 2:  stop
            drive.stop();
            // robot.gyro.init();
            idle();
        }

        sleep(5000);
    } // end of runOpMode

    private void setPowerAll(double rf, double rb, double lf, double lb){
        robot.right_front_drive.setPower(rf);
        robot.right_back_drive.setPower(rb);
        robot.left_front_drive.setPower(lf);
        robot.left_back_drive.setPower(lb);
    }
    public void autotelemetry(String status){
        robot.telemetry.addData("Status", status);
        robot.telemetry.addData("heading", robot.gyro.getHeading());
        robot.telemetry.addData("", "========== Odometry ==========");
        robot.telemetry.addData("total left ticks", "%.1f", (float)robot.odom.getTotalLeftTicks());
        robot.telemetry.addData("total right ticks", "%.1f", robot.odom.getTotalRightTicks());
        robot.telemetry.addData("center ticks", "%.1f", robot.odom.getTotalCenterTicks());
        robot.telemetry.addData("Orientation/Angle","%.3f",robot.odom.robotOrientationRadians * 57.29578f);
        robot.telemetry.addData("X (inches)","%.1f",robot.odom.robotGlobalXCoordinatePosition/robot.odom.COUNTS_PER_INCH);
        robot.telemetry.addData("Y (inches)","%.1f",robot.odom.robotGlobalYCoordinatePosition/robot.odom.COUNTS_PER_INCH);
        robot.telemetry.addData("", "========== Gyro ==========");
        //robot.telemetry.addData("Gyro Heading Radians","%.2f", robot.gyro.getHeadingRadians());
        robot.telemetry.addData("Gyro Heading","%.2f", robot.gyro.getHeadingFloat());
        robot.telemetry.addData("Gyro Calibration", robot.imu.isGyroCalibrated());
        robot.telemetry.addData("", "========== Encoder Values ==========");

        robot.telemetry.addData("Raw right front value:","%d", robot.right_front_drive.getCurrentPosition());
        robot.telemetry.addData("Raw left back value:","%d", robot.left_back_drive.getCurrentPosition());
        robot.telemetry.addData("Raw right back value:","%d", robot.right_back_drive.getCurrentPosition());
        robot.telemetry.addData("Raw left front value:","%d", robot.left_front_drive.getCurrentPosition());

        robot.telemetry.addData("leftEncoderPosition:","%d", robot.verticalLeft.getCurrentPosition());
        robot.telemetry.addData("rightEncoderPosition:","%d", robot.verticalRight.getCurrentPosition());
        robot.telemetry.addData("horizontalEncoderPosition:","%d", robot.horizontal.getCurrentPosition());

        robot.telemetry.update();
    }
} //end of OpMode
