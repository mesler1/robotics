
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.List;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;

@Autonomous(name="RotateTest", group="Pyro")

public class RotateTest extends LinearOpMode {

    PyroHardware        robot = new PyroHardware(telemetry, this);
    PyroDrive           drive = new PyroDrive(robot);
    // PyroOdom            odom = new PyroOdom(robot);

    private ElapsedTime    runtime = new ElapsedTime();

    static final double    DRIVE_SPEED            = 0.8;
    static final double    DRIVE_SPEED_INCR    = 0.05;

    int target = 0;
    float target_start = 0;
    double speed = 0.;
    double angle = 0.;
    double rotate = 0.;
    boolean touched = false;
    double thetaLimit = 90;

    @Override
    public void runOpMode()
    {

        robot.init(hardwareMap, true, true);

        drive.noencoder();

        boolean right = false;

        //init gyro
        target_start = robot.gyro.getHeadingFloat();

        // Send telemetry message to signify robot waiting;
        robot.telemetry.addData("Status", "Waiting");    //
        // robot.telemetry.addData("Heading", "%3d", target_start);    //
        robot.telemetry.update();
        waitForStart();

        sleep(500);

        runtime.reset();

        drive.runDrivePolar(0, 0, -0.1);
        while(opModeIsActive()) {
            robot.telemetry.addData("heading", robot.gyro.getHeading());
            robot.telemetry.addData("", "========== Odometry ==========");
            robot.telemetry.addData("total left ticks", "%.1f", (float)robot.odom.getTotalLeftTicks());
            robot.telemetry.addData("total right ticks", "%.1f", robot.odom.getTotalRightTicks());
            robot.telemetry.addData("center ticks", "%.1f", robot.odom.getTotalCenterTicks());
            robot.telemetry.addData("Orientation/Angle","%.3f",robot.odom.robotOrientationRadians * 57.29578f);
            robot.telemetry.addData("X (inches)","%.1f",robot.odom.robotGlobalXCoordinatePosition/robot.odom.COUNTS_PER_INCH);
            robot.telemetry.addData("Y (inches)","%.1f",robot.odom.robotGlobalYCoordinatePosition/robot.odom.COUNTS_PER_INCH);
            robot.telemetry.addData("", "========== Gyro ==========");
            //robot.telemetry.addData("Gyro Heading Radians","%.2f", robot.gyro.getHeadingRadians());
            robot.telemetry.addData("Gyro Heading","%.2f", robot.gyro.getHeadingFloat());
            robot.telemetry.addData("Gyro Calibration", robot.imu.isGyroCalibrated());
            robot.telemetry.addData("", "========== Encoder Values ==========");

            robot.telemetry.addData("Raw right front value:","%d", robot.right_front_drive.getCurrentPosition());
            robot.telemetry.addData("Raw left back value:","%d", robot.left_back_drive.getCurrentPosition());
            robot.telemetry.addData("Raw right back value:","%d", robot.right_back_drive.getCurrentPosition());
            robot.telemetry.addData("Raw left front value:","%d", robot.left_front_drive.getCurrentPosition());

            robot.telemetry.addData("leftEncoderPosition:","%d", robot.verticalLeft.getCurrentPosition());
            robot.telemetry.addData("rightEncoderPosition:","%d", robot.verticalRight.getCurrentPosition());
            robot.telemetry.addData("horizontalEncoderPosition:","%d", robot.horizontal.getCurrentPosition());
            robot.telemetry.update();
            sleep(10);
        }
    } // end of runOpMode
} //end of OpMode
