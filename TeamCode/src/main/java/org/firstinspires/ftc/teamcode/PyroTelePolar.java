
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.MotionDetection;

import java.util.Vector;


@TeleOp(name="PyroTelePolar", group="Q1")

public class PyroTelePolar extends OpMode{

    PyroHardware robot  = new PyroHardware(telemetry);
    PyroDrive drive  = new PyroDrive(robot);
    // PyroOdom odom = new PyroOdom(robot);

    
    @Override
    public void init() {
        robot.init(hardwareMap,true,true);
        drive.noencoder();
        //robot.elevator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Say", "Welcome, Lord Vader");    //




        }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
    }

    @Override
    public void loop() {
        robot.odom.updatePosition();
        drive.runDrivePolarJoy(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x);

        robot.telemetry.addData("", "========== Odometry ==========");
        robot.telemetry.addData("total left ticks", "%.1f", (float)robot.odom.getTotalLeftTicks());
        robot.telemetry.addData("total right ticks", "%.1f", robot.odom.getTotalRightTicks());
        robot.telemetry.addData("center ticks", "%.1f", robot.odom.getTotalCenterTicks());
        robot.telemetry.addData("Orientation/Angle","%.3f",robot.odom.robotOrientationRadians * 57.29578f);
        robot.telemetry.addData("X (inches)","%.1f",robot.odom.robotGlobalXCoordinatePosition/robot.odom.COUNTS_PER_INCH);
        robot.telemetry.addData("Y (inches)","%.1f",robot.odom.robotGlobalYCoordinatePosition/robot.odom.COUNTS_PER_INCH);
        robot.telemetry.addData("", "========== Gyro ==========");
        //robot.telemetry.addData("Gyro Heading Radians","%.2f", robot.gyro.getHeadingRadians());
        robot.telemetry.addData("Gyro Heading","%.2f", robot.gyro.getHeadingFloat());
        robot.telemetry.addData("Gyro Calibration", robot.imu.isGyroCalibrated());
        robot.telemetry.addData("", "========== Encoder Values ==========");

        robot.telemetry.addData("Raw right front value:","%d", robot.right_front_drive.getCurrentPosition());
        robot.telemetry.addData("Raw left back value:","%d", robot.left_back_drive.getCurrentPosition());
        robot.telemetry.addData("Raw right back value:","%d", robot.right_back_drive.getCurrentPosition());
        robot.telemetry.addData("Raw left front value:","%d", robot.left_front_drive.getCurrentPosition());

        robot.telemetry.addData("leftEncoderPosition:","%d", robot.verticalLeft.getCurrentPosition());
        robot.telemetry.addData("rightEncoderPosition:","%d", robot.verticalRight.getCurrentPosition());
        robot.telemetry.addData("horizontalEncoderPosition:","%d", robot.horizontal.getCurrentPosition());

        telemetry.update();
    }

    @Override
    public void stop() {
    }
}
    
