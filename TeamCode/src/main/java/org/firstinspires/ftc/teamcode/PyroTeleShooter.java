package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp(name="PyroTeleShooter", group="Q1")

public class PyroTeleShooter extends OpMode{

    PyroHardware robot  = new PyroHardware(telemetry);
    PyroDrive drive     = new PyroDrive(robot);
    PyroOdom odom       = new PyroOdom(robot);

    static double GATECLOSED = 0.5;
    static double GATEOPEN = 1.;
    static double CONVEYORSPEED = 1.;
    static double INTAKESPEED = 1.;
    static double SHOOTERSPEED = .85;

    @Override
    public void init() {
        robot.init(hardwareMap,true,false);
        drive.noencoder();

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Say", "Welcome, Lord Vader");
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
    }

    @Override
    public void loop() {

        drive.runDrivePolarJoySmooth(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x, this);

        /*
        if (gamepad1.right_bumper) {
            robot.shooterFront.setPower(SHOOTERSPEED);
            robot.shooterRear.setPower(SHOOTERSPEED);
        }
        else {
            robot.shooterFront.setPower(0.);
            robot.shooterRear.setPower(0.);
        }

        if (gamepad1.left_bumper) {
            robot.intake.setPower(INTAKESPEED);
            robot.conveyor.setPower(CONVEYORSPEED);
        }
        else {
            robot.intake.setPower(0.);
            robot.conveyor.setPower(0.);
        }

        if (gamepad1.x) {
            robot.gate.setPosition(GATEOPEN);
        }
        else {
            robot.gate.setPosition(GATECLOSED);
        }
        */
    }

    @Override
    public void stop() {
    }
}